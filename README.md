# DESCRIPTION

Object to define the Requirements for a theme or plugin.

## Usage:

```
<?php

 use Baxtian\WP_Requirements as WP_Requirements;

 // Do we miss a requirement
 if (WP_Requirements::requirements(
 	'Theme or Plugin Name', 
 	[
 		[
 			'name' => 'Timber',
 			'slug' => 'timber-library/timber.php',
 			'zip'=> 'https://downloads.wordpress.org/plugin/timber-library.latest-stable.zip',
 			'check' => ['self::has_timber', ''], // To detect if your theme has timber.
 		],
 		[
 			'name' => 'SCSS-Library',
 			'slug' => 'scss-library/scss-library.php',
 			'zip'=> 'https://downloads.wordpress.org/plugin/scss-library.latest-stable.zip',
 			'check' => ['class_exists', 'ScssLibrary\ScssLibrary'], // You can use here also 'function_exists'.
 		],
 		[
 			'name' => 'Font Awesome',
 			'slug' => 'font-awesome/font-awesome.php',
 			'zip'=> 'https://downloads.wordpress.org/plugin/font-awesome.latest-stable.zip',
 			'check' => ['class_exists', 'FortAwesome\FontAwesome_Loader'],
 		],
 	]
 )) {

	// Code to be displayed if there are missing requirements
	...
	
 	// Finish your theme execution? It's up to you.
 	return;
 }
```

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

## 0.3.10

Check directly if the plugin is activated using the slug if not check has been sent.

## 0.3.9

Use the timber directory and not the composer.json file as a compliance detector for the 'has_timber' requirement.

## 0.3.8

Warning if theme and old timber plugin are both active.

## 0.3.6

* Replace self:: to Baxtian\WP_Requirement:: if used as check function

## 0.3.5

* Message to deatcivate Old Timber Plugin. 

## 0.3.3

* Action to detect if the theme has Timber 2. 

## 0.3

* In case of using this class in multiple providers, 
  allow Composer to set which file to use by default. 

## 0.2

* Allow to use PHP8.0

## 0.1

* First stable release
