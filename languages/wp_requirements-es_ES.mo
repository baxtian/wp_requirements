��          \      �       �   "   �      �   U   �     R  i  i  %   �     �  :  y      �     �  s   �  @  Y  �  �  8   7
  �   p
                                       %s has been activated succesfully. %s requires %s. %s requires %s. Use this link to automatically <a href='%s'>install requirements</a>. %s requires a Timber-based theme and your site uses the old Timber plugin. <strong>Please note</strong> that the Timber plugin is based on Timber 1.x and therefore may generate errors and warning messages. <strong>It is highly recommended to use a Timber 2 based theme.</strong> %s requires a Timber-based theme. Either install a theme that uses Timber 2 or use this link to automatically <a href='%s'>install the old Timber plugin</a>. <strong>Please note</strong> that the Timber plugin is based on Timber 1.x and therefore may generate errors and warning messages. <strong>It is highly recommended to use a Timber 2 based theme.</strong> Couldn't install %s. Try it manually. The active theme uses Timber 2 but the old Timber plugin is active. <a href='%s'>Disable the plugin</a> to fix possible errors. Project-Id-Version: wp_requirements
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-05 16:20-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ..
X-Generator: Poedit 3.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
 %s se ha activado correctamente. %s requiere %s. %s requiere %s. Use el siguiente enlace para <a href='%s'>instalar los plugins requeridos</a> en forma automática. %s requiere un tema basado en Timber y su sitio utiliza el antiguo complemento de Timber. <strong>Tenga en cuenta</strong> que el complemento Timber se basa en Timber 1.x y, por lo tanto, puede generar errores y mensajes de advertencia. <strong>Se recomienda encarecidamente utilizar un tema basado en Timber 2.</strong> %s requiere un tema basado en Timber. Instale un tema que utilice Timber 2 o utilice este enlace para <a href='%s'>instalar automáticamente el antiguo complemento de Timber</a>. <strong>Tenga en cuenta</strong> que el complemento de Timber es basado en Timber 1.x y por lo tanto puede generar errores y mensajes de advertencia. <strong>Se recomienda encarecidamente utilizar un tema basado en Timber 2.</strong> No se ha podido instalar %s. Inténtelo en forma manual. El tema activo usa Timber 2 pero el antiguo complemento de Timber está activo. <a href='%s'>Deshabilite el complemento</a> para corregir posibles errores. 