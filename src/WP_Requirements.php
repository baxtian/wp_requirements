<?php
namespace Baxtian;

use Plugin_Upgrader;

/**
 * Administrar los plugins requeridos
 */
class WP_Requirements
{
	private $requisitor;
	private $name;
	private $slug;
	private $zip;

	const TIMBER_FILE = 'timber-library/timber.php';

	/**
	 * Constructor para crear unba instancia del plugin requerido
	 * @param string $requisitor Nombre de la plantilla o plugin que solicita este requerimiento
	 * @param string $name       Nombre del plugin
	 * @param string $slug       Identificador del plugin en el formato directorio/archivo-plugin.php
	 * @param string $zip        Enlace a la versión más reciente del plugin
	 * @param array  $check_func Arreglo con callback para determinar si está o no instalado
	 */
	private function __construct($requisitor, $name, $slug, $zip, $check_func)
	{
		$this->requisitor = $requisitor;
		$this->name       = $name;
		$this->slug       = $slug;
		$this->zip        = $zip;
	}

	/**
	 * Revisa el estado del plugin y anexa mensajes para ser mostrados en la
	 * cabecera del escritorio de administración. Retorna verdadero en caso de
	 * haber publicado un mensaje o si ya había publicado anteriormente uno.
	 * El objetivo de esta función es filtrar los mensajes para que solo se vea un mensaje
	 * por dependencia y solo en el primero se vea el mensaje de instalación.
	 * @param  boolean $indicador_requerimiento_enviado ¿Mostrar el mensaje de dependencia?
	 * @return boolean                                  Nuevo estado del mensaje de dependencia.
	 */
	private function check($indicador_requerimiento_enviado = false)
	{
		// Variable global para saber si ya mostramos el mensaje para instalar requerimientos
		global $enlace_instalador_enviado;

		// ¿Se debe o no mostrar el mensaje? Es posible que ya se haya mostrado
		// el mensaje que indica la dependnecia así que no es necesario mostrarlo
		// nuevamente.
		if (!$indicador_requerimiento_enviado) {
			// Ya podemos dar por cierto que se va a mostrar el mensaje de
			// requrimiento de esta dependencia.
			$indicador_requerimiento_enviado = true;

			// ¿Estámos instalando los requerimientos?
			if (isset($_GET['req']) && $_GET['req'] == 'install') {
				add_action('admin_notices', function () {
					// Si se puede reemplazar/instalar el plugin indicamos que se
					// ha podido instalar correctamente
					if ($this->replace()) {
						$text = sprintf(__('%s has been activated succesfully.', 'wp_requirements'), $this->name);
						printf('<div class="notice notice-success is-dismissible"><p>%s</p></div>', $text);
					} else {
						// En caso de error indicar que es necesario instalar manualmente
						$text = sprintf(__("Couldn't install %s. Try it manually.", 'wp_requirements'), $this->name);
						printf('<div class="error"><p>%s</p></div>', $text);
					}
				});
			} else { //Aun no estamos instalando, así que indicaremos los requerimientos
				//Si no está activo el plugin,
				if (!$this->is_active()) {
					// Ya se envió el mensaje para instalar los requerimientos
					// así que solo se muestra la dependencia
					if (isset($enlace_instalador_enviado) && $enlace_instalador_enviado) {
						add_action('admin_notices', function () {
							$text = sprintf(__('%s requires %s.', 'wp_requirements'), $this->requisitor, $this->name);
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					} elseif($this->name == 'Timber') { // Aun no se ha enviado el mensaje con el link para instalar los requerimientos
						// Si es timber, mostrar la infiormación especial.
						$enlace_instalador_enviado = true;

						//Crear el mensaje con el enlace para instalar los requerimientos
						add_action('admin_notices', function () {
							$text = sprintf(__("%s requires a Timber-based theme. Either install a theme that uses Timber 2 or use this link to automatically <a href='%s'>install the old Timber plugin</a>. <strong>Please note</strong> that the Timber plugin is based on Timber 1.x and therefore may generate errors and warning messages. <strong>It is highly recommended to use a Timber 2 based theme.</strong>", 'wp_requirements'), $this->requisitor, esc_url(admin_url('plugins.php?req=install')));
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					} else { // Aun no se ha enviado el mensaje con el link para instalar los requerimientos
						// Dar por supuesto que ya vamos a enviar este enlace.
						$enlace_instalador_enviado = true;

						//Crear el mensaje con el enlace par ainstalar los requerimientos
						add_action('admin_notices', function () {
							$text = sprintf(__("%s requires %s. Use this link to automatically <a href='%s'>install requirements</a>.", 'wp_requirements'), $this->requisitor, $this->name, esc_url(admin_url('plugins.php?req=install')));
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					}
				} else {
					if($this->name == 'Timber') { // Aun no se ha enviado el mensaje con el link para instalar los requerimientos
						// Si es timber, mostrar la infiormación especial.
						$enlace_instalador_enviado = true;

						//Crear el mensaje con el enlace para instalar los requerimientos
						add_action('admin_notices', function () {
							$text = sprintf(__('%s requires a Timber-based theme and your site uses the old Timber plugin. <strong>Please note</strong> that the Timber plugin is based on Timber 1.x and therefore may generate errors and warning messages. <strong>It is highly recommended to use a Timber 2 based theme.</strong>', 'wp_requirements'), $this->requisitor);
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					}
				}
				// El caso contratía indicaría que está activo, así que es posible
				// que al ser requerido por otro plugin ya se haya instalado así
				// que podemos pasar este mensaje porque la dependencia ya se cumplió.
			}
		}

		return $indicador_requerimiento_enviado;
	}

	/**
	 * Reemplazar el plugin.
	 * @return boolean Estado del reemplazo.
	 */
	private function replace()
	{
		$status = false;

		if ($this->is_installed()) {
			$this->upgrade();
			$installed = true;
		} else {
			$installed = $this->install();
		}

		if (!is_wp_error($installed) && $installed) {
			$activate = activate_plugin($this->slug);

			if (is_null($activate)) {
				$status = true;
			}
		}

		return $status;
	}

	/**
	 * ¿Está activo el plugin?
	 * @return boolean ¿Está activo el plugin?
	 */
	private function is_active()
	{
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');

		return is_plugin_active($this->slug);
	}

	/**
	 * ¿Está instalado el plugin?
	 * @return boolean ¿Está instalado el plugin?
	 */
	private function is_installed()
	{
		if (! function_exists('get_plugins')) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$all_plugins = get_plugins();

		if (!empty($all_plugins[$this->slug])) {
			return true;
		}

		return false;
	}

	/**
	 * Instalar el plugin
	 * @return boolean ¿Se pudo instalar el plugin?
	 */
	private function install()
	{
		require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		wp_cache_flush();

		$upgrader  = new Plugin_Upgrader();
		$installed = $upgrader->install($this->zip);

		return $installed;
	}

	/**
	 * Determinar si contamos con timber en la plantilla
	 *
	 * @return boolean
	 */
	private static function has_timber()
	{
		$dir = get_template_directory() . DIRECTORY_SEPARATOR . 'vendor' .
			DIRECTORY_SEPARATOR . 'timber' .
			DIRECTORY_SEPARATOR . 'timber';

		return (is_dir($dir));
	}

	/**
	 * Actualizar el plugin
	 * @return boolean ¿Se pudo actualizar el plugin?
	 */
	private function upgrade()
	{
		require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		wp_cache_flush();

		$upgrader = new Plugin_Upgrader();
		$upgraded = $upgrader->upgrade($this->slug);

		return $upgraded;
	}

	/**
	 * Ejecuta el callback. En caso d eincluir un self:: lo reemplaza por la clase.
	 *
	 * @param string $callback	Callback
	 * @param string $args		Argumentos
	 * @return bool Respuesta del callback
	 */
	private static function execute($callback, $args)
	{
		$callback = str_replace('self::', self::class . '::', $callback);

		return call_user_func($callback, $args);
	}

	/**
	 * Función para declarar los plugins requeridos
	 * @param  array $plugins Conjunto de plugins requeridos srcset. Cada fila corresponde a un plugin,
	 *                        y contendrá 3 items: 'name' para el nombre, 'slug' para el Identificador
	 *                        del plugin en el formato directorio/archivo-plugin.php, zip para el enlace
	 *                        a la versión más reciente del plugin y check para determinar cómo se sabrá si
	 *                        el plugin está instalado
	 *                        Ejemplo: array(
	 *                        	array(
	 *                        		'name' => 'Timber',
	 *                        		'slug' => 'timber-library/timber.php',
	 *                        		'zip'=> 'https://downloads.wordpress.org/plugin/timber-library.latest-stable.zip',
	 *                        		'check' => [ 'class_exists', 'Timber' ]
	 *                        	),
	 *                        	array(
	 *                        		'name' => 'SCSS-Library',
	 *                        		'slug' => 'scss-library/scss-library.php',
	 *                        		'zip'=> 'https://downloads.wordpress.org/plugin/scss-library.latest-stable.zip',
	 *                        		'check' => [ 'class_exists', 'ScssLibrary' ]
	 *                        	),
	 *                        )
	 */
	public static function requirements($requisitor, $plugins)
	{
		// Inicializar bandera de falta de requerimientos
		$faltan_requerimientos = false;

		// Si están activos todos los requerimientos no hay que seguir
		foreach ($plugins as $plugin) {
			if (isset($plugin['check']) && is_array($plugin['check'])) {
				$call = $plugin['check'][0];
				$args = $plugin['check'][1];

				if (!self::execute($call, $args)) {
					$faltan_requerimientos = true;
				}
			} else {
				$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));
				if(!in_array($plugin['slug'], $active_plugins)) {
					$faltan_requerimientos = true;
				}
			}
		}

		// Variable global con el arreglo para indicar si ya informamos sobre la
		// dependencia de un plugin particular.
		// Se declara global para compartir estos estados con otros manejadores
		// de dependencias.
		global $enlace_instalar;

		// Inicializar el arreglo en caso de ser necesario
		if (!$enlace_instalar) {
			$enlace_instalar = [];
		}

		// Inicializar el sistema de traducción
		$domain = 'wp_requirements';
		$locale = apply_filters('plugin_locale', determine_locale(), $domain);

		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$path         = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile       = $path . $domain . '-' . $locale . '.mo';

		load_textdomain($domain, $mofile);

		// Detectar si el tema tiene Timber 2 y está activo el plugin Timber
		if(self::has_timber() && !isset($enlace_instalar['timber2-timber'])) {
			include_once(ABSPATH . 'wp-admin/includes/plugin.php');
			if(is_plugin_active(self::TIMBER_FILE)) {
				$enlace_instalar['timber2-timber'] = true;
				add_action('admin_notices', function () {
					$url = sprintf('plugins.php?%s', http_build_query([
						'action' => 'deactivate',
						'plugin' => self::TIMBER_FILE,
						's'      => 'timber',
					]));
					$url  = wp_nonce_url($url, 'deactivate-plugin_' . self::TIMBER_FILE);
					$text = sprintf(__("The active theme uses Timber 2 but the old Timber plugin is active. <a href='%s'>Disable the plugin</a> to fix possible errors.", 'wp_requirements'), $url);
					printf('<div class="error"><p>%s</p></div>', $text);
				});
			}
		}

		// Si no faltan requerimientos, no es necesario continuar
		if (!$faltan_requerimientos) {
			return false;
		}

		// Recorrer los requerimientos
		foreach ($plugins as $plugin) {
			// Verificar que la función que teremina si está o no activo
			// el requerimiento retorne false (no está activo)
			if(isset($plugin['check'])) {
				$call = $plugin['check'][0];
				$args = $plugin['check'][1];
				if (!self::execute($call, $args)) {
					continue;
				}
			} else {
				$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));
				if(!in_array($plugin['slug'], $active_plugins)) {
					continue;
				}
			}
		}

		// Inicializar la variable que tendrá la bandera que indica si ya se mostró
		// el enlace para instalar este plugin
		if (!isset($enlace_instalar[$plugin['name']])) {
			$enlace_instalar[$plugin['name']] = false;
		}

		// Inicializar instalador de plugin.
		$plug = new WP_Requirements($requisitor, $plugin['name'], $plugin['slug'], $plugin['zip'], $plugin['check'] ?? false);

		// Ejecutar instalador y guardar el estado que retorne en el arreglo
		// que guarda la bandera que indica si ya se mostró o no el mensaje sobre
		// de dependencia de este plugin.
		$enlace_instalar[$plugin['name']] = $plug->check($enlace_instalar[$plugin['name']]);

		return true;
	}
}
